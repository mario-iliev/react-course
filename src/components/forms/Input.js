import React from "react";
import styled from "styled-components";

import Label from "./Label";

const FormInput = props => (
  <InputRow className={`form-row${props.error ? " error" : ""}${props.disabled ? " disabled" : ""}`}>
    {props.label && <Label label={props.label} error={props.error} />}

    <input
      ref={props.node}
      name={props.name}
      type={props.type}
      value={props.value}
      placeholder={props.placeholder}
      onChange={props.onChange}
      onBlur={props.onBlur}
      disabled={props.disabled}
      autoFocus={props.autofocus}
      autoCapitalize={props.autocapitalize}
      autoCorrect={props.autocorrect}
      autoComplete={props.autocomplete}
      spellCheck={props.spellcheck}
      pattern={props.pattern}
      min={props.min}
      max={props.max}
      step={props.step}
      checked={props.checked}
      inputMode={props.inputmode}
    />
  </InputRow>
);

export default FormInput;

const InputRow = styled.div`
  position: relative;
  margin: 20px 0 0 0;

  &.error {
    label,
    input {
      color: #ff3300;
    }
    input,
    input:focus {
      border-color: #ff3300;
    }
  }

  input {
    display: block;
    width: 100%;
    height: 45px;
    max-width: 100%;
    color: #112146;
    font-size: 15px;
    font-weight: 100;
    outline: none;
    margin: 5px 0 0 0;
    padding: 0 10px;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #909090;
    background-color: #fff;
    transition: border-color 0.3s ease, background-color 0.3s ease;

    &:focus {
      border-color: #0087d2;
    }
    &[disabled] {
      color: #999;
      border-color: #ccc;
      background-color: #fbfbfb;

      &:-webkit-autofill {
        -webkit-text-fill-color: #999;
        -webkit-box-shadow: 0 0 0 30px #fbfbfb inset;
      }
    }
    &:-webkit-autofill {
      -webkit-text-fill-color: #112146;
      -webkit-box-shadow: 0 0 0 30px #fff inset;
    }
    &::-webkit-input-placeholder {
      color: #888888;
      -webkit-text-fill-color: #888888;
    }
    &::-moz-placeholder {
      color: #888888;
      -webkit-text-fill-color: #888888;
    }
    &:-ms-input-placeholder {
      color: #888888;
      -webkit-text-fill-color: #888888;
    }
    &:-moz-placeholder {
      color: #888888;
      -webkit-text-fill-color: #888888;
    }
  }
`;
