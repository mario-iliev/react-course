import React from "react";

const ListItem = props => (
  <li>
    <a href={props.link} target={props.target} onClick={props.onClick} rel={props.target === "_blank" && "noopener noreferrer"}>
      {props.text || "link"}
    </a>
  </li>
);

export default ListItem;
