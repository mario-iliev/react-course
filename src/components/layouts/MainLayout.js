import React, { Fragment } from "react";

import Menu from "../header/Menu";
import LogoTop from "../header/LogoTop";
import HamburgerButton from "../header/HamburgerButton";

const MainLayout = props => (
  <Fragment>
    <LogoTop />
    <HamburgerButton />
    <Menu />
    {props.children}
  </Fragment>
);

export default MainLayout;
