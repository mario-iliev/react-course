import React from "react";
import styled from "styled-components";

const FormNode = props => (
  <Form onSubmit={props.onSubmit} noValidate>
    {props.success}
    {props.error}
    <div className="rows-wrap">{props.children}</div>
  </Form>
);

export default FormNode;

const Form = styled.form`
  display: table;
  width: 100%;
  max-width: 670px;
  margin: 0 auto;

  .form-row:first-child {
    margin-top: 0;
  }
`;
