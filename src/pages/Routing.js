import React from "react";

import SectionScroller from "../components/content/section-scroller/Basic";
import CodeHighlight from "../components/content/CodeHighlight";
import ReadMore from "../components/content/ReadMore";

import RoutingGIF from "../images/gif/routing.gif";

const PageRouting = props => {
  const content = [
    {
      section: [
        <div className="content" key="rt-choices">
          <div className="underline">
            <strong>Routing.</strong> Choose your weapon!
          </div>
          <img className="gif-img" src={RoutingGIF} alt="" />
          <ReadMore link="https://reactjs.org/community/routing.html" />
        </div>
      ]
    },
    {
      section: [
        <div className="content" key="rt-choices">
          <div className="underline">React Router</div>
          <CodeHighlight>{`
<Router>
  <Link to="/">Home</Link>
  <Link to="/about">About</Link>
  <Link to="/topics">Topics</Link>

  <Route exact path="/" component={Home} />
  <Route path="/about" component={About} />
  <Route path="/topics" component={Topics} />
  <Route component={404NotFound} />
</Router>
          `}</CodeHighlight>
          <ReadMore link="https://reacttraining.com/react-router/web/example/basic" />
        </div>
      ]
    },
    {
      section: [
        <div className="content" key="rt-htaccess">
          <div className="underline">Don't forget the server part.</div>
          <CodeHighlight>{`
# Allow direct URL access for react app
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.html [QSA,L]
          `}</CodeHighlight>
        </div>
      ]
    }
  ];

  return <SectionScroller content={content} />;
};
export default PageRouting;
