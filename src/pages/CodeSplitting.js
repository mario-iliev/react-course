import React from "react";

import SectionScroller from "../components/content/section-scroller/Basic";
import CodeHighlight from "../components/content/CodeHighlight";
import ReadMore from "../components/content/ReadMore";

const CodeSplitting = props => {
  const content = [
    {
      section: [
        <div className="content" key="split-intro">
          <div className="underline">Code-Splitting</div>
          <p>
            It's useful for single page applications and a <strong>must</strong> for a React based websites.
          </p>
          <p>
            You don't want users to load <strong>all</strong> of your app code in order to see a specific page right? <br />There are several approaches to resolve this
            situation.
          </p>
          <p>
            <strong>Next.js</strong> and <strong>React Loadable</strong>
            <br /> are listed below as a more commonly used approaches.
          </p>
          <ReadMore link="https://reactjs.org/docs/code-splitting.html" />
        </div>
      ]
    },
    {
      anchor: "next-js",
      section: [
        <div className="content" key="split-next">
          <div className="underline">Next.js</div>
          <p>"A lightweight framework for static and server‑rendered applications."</p>
          <p>It work's really simple. Just create a "pages" folder, where each .js file (React component) will be used as a route.</p>
          <CodeHighlight>{`
/pages
  index.js
  About-us.js
          `}</CodeHighlight>
          <ReadMore link="https://nextjs.org/docs/#setup" />
        </div>
      ]
    },
    {
      anchor: "react-loadable",
      section: [
        <div className="content" key="react-loadable">
          <div className="underline">React Loadable</div>
          <p>
            It's a very easy to use library. Especially if you are using <strong>React Router</strong>
            <br />
            <i>This presentation is using React Loadable</i>
          </p>
          <CodeHighlight>{`
import Loadable from 'react-loadable';

const Home = Loadable({
  loader: () => import('./routes/Home'),
  loading: LoadingComponent,
});

const About = Loadable({
  loader: () => import('./routes/About'),
  loading: LoadingComponent,
});

const App = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Home}/>
      <Route path="/about" component={About}/>
    </Switch>
  </Router>
);
          `}</CodeHighlight>
          <ReadMore link="https://github.com/jamiebuilds/react-loadable" />
        </div>
      ]
    }
  ];

  return <SectionScroller content={content} />;
};
export default CodeSplitting;
