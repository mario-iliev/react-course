import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

import LogoSVG from "../../images/LogoSVG";

const LogoTop = props => (
  <Logo to="/" className="logo-top">
    <LogoSVG />
  </Logo>
);

export default LogoTop;

const Logo = styled(Link)`
  position: fixed;
  cursor: pointer;
  top: 18px;
  left: 20px;
  z-index: 1;

  svg {
    display: block;
    width: 40px;
    width: 40px;
    fill: #909090;
    transition: fill 0.3s ease;

    &:hover {
      fill: #17bed0;
    }
  }
`;
