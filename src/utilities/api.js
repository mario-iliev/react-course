const api = {
  localStorage: {
    set: (key, data) => {
      if (typeof data === "object") {
        data = JSON.stringify(data);
      }

      localStorage.setItem(key, data);
    },
    get: key => {
      let result;

      try {
        result = JSON.parse(localStorage.getItem(key));
      } catch (e) {
        result = localStorage.getItem(key);
      }

      return result;
    },
    remove: key => localStorage.removeItem(key)
  },
  isEmailValid: email => {
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(String(email).toLowerCase());
  },
  scrollTo: (destination, duration = 200, easing = "linear", callback) => {
    const easings = {
      linear: t => t,
      easeInQuad: t => t * t,
      easeOutQuad: t => t * (2 - t),
      easeInOutQuad: t => (t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t),
      easeInCubic: t => t * t * t,
      easeOutCubic: t => --t * t * t + 1,
      easeInOutCubic: t => (t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1),
      easeInQuart: t => t * t * t * t,
      easeOutQuart: t => 1 - --t * t * t * t,
      easeInOutQuart: t => (t < 0.5 ? 8 * t * t * t * t : 1 - 8 * --t * t * t * t),
      easeInQuint: t => t * t * t * t * t,
      easeOutQuint: t => 1 + --t * t * t * t * t,
      easeInOutQuint: t => (t < 0.5 ? 16 * t * t * t * t * t : 1 + 16 * --t * t * t * t * t)
    };
    const start = window.pageYOffset;
    const startTime = "now" in window.performance ? performance.now() : new Date().getTime();
    const documentHeight = Math.max(
      document.body.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.clientHeight,
      document.documentElement.scrollHeight,
      document.documentElement.offsetHeight
    );
    const windowHeight = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName("body")[0].clientHeight;
    const destinationOffset = typeof destination === "number" ? destination : destination.offsetTop;
    const destinationOffsetToScroll = Math.round(documentHeight - destinationOffset < windowHeight ? documentHeight - windowHeight : destinationOffset);

    if ("requestAnimationFrame" in window === false) {
      window.scroll(0, destinationOffsetToScroll);

      if (typeof callback === "function") {
        callback();
      }

      return;
    }

    function scroll() {
      const now = "now" in window.performance ? performance.now() : new Date().getTime();
      const time = Math.min(1, (now - startTime) / duration);
      const timeFunction = easings[easing](time);
      const scrollPosition = Math.ceil(timeFunction * (destinationOffsetToScroll - start) + start);

      window.scroll(0, scrollPosition);

      if (window.pageYOffset === destinationOffsetToScroll) {
        if (typeof callback === "function") {
          callback();
        }
        return;
      }

      requestAnimationFrame(scroll);
    }

    scroll();
  },
  createArrayMatrix: (x, y) => Array.from({ length: y || x }, () => new Array(x).fill(0))
};

export default api;
