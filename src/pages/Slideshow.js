import React, { Component } from "react";
import styled from "styled-components";

import DefaultSlideShow from "../components/content/slideshow/Default";
import SelectList from "../components/content/SelectList";
import Input from "../components/forms/Input";

/*
  OK, calm down. You don't need this heavy code to use the slideshow component. This is just a showcase page.
  In the end all you need is:
  <DefaultSlideShow data={arrayWithURLS} config={ofYourChoice} />
*/

class Slideshow extends Component {
  constructor() {
    super();

    this.state = {
      prettyConfig: true,
      defaultConfig: false,
      mainNavigation: true,
      bottomNavigation: true,
      bottomNavigationThumbs: false,
      bottomNavigationInside: false,
      slideText: true,
      autoSlide: true,
      pauseOnHover: false,
      transitionType: 5,
      slidesFullHeight: true,
      clearStyles: false,
      draggable: true,
      dragChangeWhen: false,
      transitionDuration: "",
      autoSlideInterval: 3000,
      disableKeyboardNavigation: false,
      youtubeThumbQuality: "max",
      backgroundColor: ""
    };
    this.prettyConfig = JSON.parse(JSON.stringify(this.state));

    this.defaultConfig = {
      prettyConfig: false,
      defaultConfig: true,
      mainNavigation: false,
      bottomNavigation: false,
      bottomNavigationThumbs: false,
      bottomNavigationInside: false,
      slideText: false,
      autoSlide: true,
      pauseOnHover: false,
      transitionType: false,
      slidesFullHeight: false,
      clearStyles: true,
      draggable: false,
      dragChangeWhen: false,
      transitionDuration: "",
      autoSlideInterval: 3000,
      disableKeyboardNavigation: false,
      youtubeThumbQuality: "max",
      backgroundColor: ""
    };

    this.exampleJSONData = [
      {
        title: "Love the nature",
        url: "https://drupal.marioiliev.com/sites/default/files/2018-03/pexels-photo-207962_2.jpeg"
      },
      {
        url: "https://www.youtube.com/watch?v=Z_fNOPwGI7k"
      },
      {
        title: "The Earth is flat",
        url: "https://drupal.marioiliev.com/sites/default/files/2018-03/pexels-photo-235615_2.jpeg"
      },
      {
        title: "Avatarish",
        url: "https://drupal.marioiliev.com/sites/default/files/2018-03/pexels-photo-326055_2.jpeg"
      },
      {
        title: "Where is it?",
        url: "https://drupal.marioiliev.com/sites/default/files/2018-03/pexels-photo-460775_2.jpeg"
      }
    ];

    this.toggleFalseTrue = [
      {
        value: false,
        text: "false"
      },
      {
        value: true,
        text: "true"
      }
    ];

    this.toggleTrueFalse = [
      {
        value: true,
        text: "true"
      },
      {
        value: false,
        text: "false"
      }
    ];

    this.examples = {
      effects: [
        {
          value: false,
          text: "none"
        },
        {
          value: 1,
          text: "Fade Out"
        },
        {
          value: 2,
          text: "Fade In"
        },
        {
          value: 3,
          text: "Slide Horizontal Hide"
        },
        {
          value: 4,
          text: "Slide Horizontal Hide & Show"
        },
        {
          value: 5,
          text: "Slide Horizontal Hide slower & Show"
        },
        {
          value: 6,
          text: "Slide Vertical Hide"
        },
        {
          value: 7,
          text: "Slide Vertical Hide & Show"
        },
        {
          value: 8,
          text: "Zoom Out Fading"
        },
        {
          value: 9,
          text: "Zoom In Fading"
        }
      ],
      youtubeThumbQuality: [
        {
          value: "low",
          text: "low"
        },
        {
          value: "medium",
          text: "medium"
        },
        {
          value: "good",
          text: "good"
        },
        {
          value: "high",
          text: "high"
        },
        {
          value: "max",
          text: "max"
        }
      ],
      draggable: this.toggleFalseTrue,
      autoSlide: this.toggleTrueFalse,
      slideText: this.toggleFalseTrue,
      bottomNavigation: this.toggleFalseTrue,
      bottomNavigationThumbs: this.toggleFalseTrue,
      pauseOnHover: this.toggleFalseTrue,
      disableKeyboardNavigation: this.toggleFalseTrue,
      bottomNavigationInside: this.toggleFalseTrue,
      mainNavigation: this.toggleFalseTrue,
      slidesFullHeight: this.toggleFalseTrue,
      clearStyles: this.toggleTrueFalse
    };
  }

  changeTransitionType = value => {
    let newState = {
      transitionType: value,
      prettyConfig: false,
      defaultConfig: false
    };

    if (value !== 3 && value !== 4 && value !== 5) {
      newState["draggable"] = false;
    }

    this.setState(newState);
  };

  changeDraggable = value => {
    this.setState({
      draggable: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeAutoSlide = value => {
    this.setState({
      autoSlide: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeSlideText = value => {
    this.setState({
      slideText: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeBottomNavigation = value => {
    this.setState({
      bottomNavigation: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeBottomNavigationThumbs = value => {
    this.setState({
      bottomNavigation: true,
      bottomNavigationThumbs: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeTransitionDuration = e => {
    const value = e.target.value > 5000 ? "" : e.target.value;

    this.setState({
      transitionDuration: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeTransitionDurationBlur = e => {
    const value = e.target.value;

    if (value < 100 && value.length) {
      this.setState({
        transitionDuration: 400
      });
    }
  };

  changeAutoSlideInterval = e => {
    this.setState({
      autoSlideInterval: e.target.value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeDragChangeWhen = e => {
    const value = e.target.value > 100 ? "" : e.target.value;

    this.setState({
      dragChangeWhen: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeBackgroundColor = e => {
    this.setState({
      backgroundColor: String(e.target.value),
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changePauseOnHover = value => {
    this.setState({
      pauseOnHover: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeDisableKeyboardNavigation = value => {
    this.setState({
      disableKeyboardNavigation: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeBottomNavigationInside = value => {
    this.setState({
      bottomNavigation: true,
      bottomNavigationInside: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeMainNavigation = value => {
    this.setState({
      mainNavigation: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeSlidesFullHeight = value => {
    this.setState({
      slidesFullHeight: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeClearStyles = value => {
    this.setState({
      clearStyles: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  changeYoutubeThumbQuality = value => {
    this.setState({
      youtubeThumbQuality: value,
      prettyConfig: false,
      defaultConfig: false
    });
  };

  setPrettyConfig = () => {
    this.setState(this.prettyConfig);
  };

  setDefaultConfig = () => {
    this.setState(this.defaultConfig);
  };

  render() {
    return (
      <div className="section">
        <div className="vertical-align">
          <Wrap>
            <DefaultSlideShow data={this.exampleJSONData} config={this.state} />

            <SelectWrap>
              <div className="vertical-align">
                <ButtonsWrap>
                  <ConfigButton className={`${this.state.prettyConfig ? "current" : ""}`} onClick={this.setPrettyConfig}>
                    Pretty config
                  </ConfigButton>
                  <ConfigButton className={`${this.state.defaultConfig ? "current" : ""}`} onClick={this.setDefaultConfig}>
                    Default config
                  </ConfigButton>
                </ButtonsWrap>
                <SelectBox>
                  <Separator style={{ marginTop: 0 }}>EFFECTS</Separator>
                  <SelectList label="Transition effect" options={this.examples.effects} current={this.state.transitionType} onOptionClick={this.changeTransitionType} />
                  <Input
                    label="Transition Duration"
                    type="number"
                    placeholder="Milliseconds"
                    step="100"
                    min="100"
                    max="5000"
                    onBlur={this.changeTransitionDurationBlur}
                    onChange={this.changeTransitionDuration}
                    value={this.state.transitionDuration}
                  />

                  <Separator>FUNCTIONALITIES</Separator>
                  <SelectList label="Automatic slide" options={this.examples.autoSlide} current={this.state.autoSlide} onOptionClick={this.changeAutoSlide} />
                  <Input
                    label="Auto slide interval"
                    type="number"
                    min="1000"
                    max="10000"
                    step="100"
                    placeholder="Milliseconds"
                    onChange={this.changeAutoSlideInterval}
                    value={this.state.autoSlideInterval}
                  />
                  <SelectList label="Draggable" options={this.examples.draggable} current={this.state.draggable} onOptionClick={this.changeDraggable} />
                  <Input
                    label="Drag change when"
                    type="number"
                    placeholder="Percent 0-100"
                    min="10"
                    max="90"
                    onChange={this.changeDragChangeWhen}
                    value={this.state.dragChangeWhen}
                  />
                  <SelectList label="Pause on hover" options={this.examples.pauseOnHover} current={this.state.pauseOnHover} onOptionClick={this.changePauseOnHover} />
                  <SelectList
                    label="Disable keyboard navigation"
                    options={this.examples.disableKeyboardNavigation}
                    current={this.state.disableKeyboardNavigation}
                    onOptionClick={this.changeDisableKeyboardNavigation}
                  />

                  <Separator>NAVIGATIONS</Separator>
                  <SelectList
                    label="Arrow navigation"
                    options={this.examples.mainNavigation}
                    current={this.state.mainNavigation}
                    onOptionClick={this.changeMainNavigation}
                  />
                  <SelectList
                    label="Bottom navigation"
                    options={this.examples.bottomNavigation}
                    current={this.state.bottomNavigation}
                    onOptionClick={this.changeBottomNavigation}
                  />
                  <SelectList
                    label="Bottom thumbs navigation"
                    options={this.examples.bottomNavigationThumbs}
                    current={this.state.bottomNavigationThumbs}
                    onOptionClick={this.changeBottomNavigationThumbs}
                  />
                  <SelectList
                    label="Bottom navigation inside"
                    options={this.examples.bottomNavigationInside}
                    current={this.state.bottomNavigationInside}
                    onOptionClick={this.changeBottomNavigationInside}
                  />

                  <Separator>CAPTIONS</Separator>
                  <SelectList label="Slides title" options={this.examples.slideText} current={this.state.slideText} onOptionClick={this.changeSlideText} />

                  <Separator>STYLING</Separator>
                  <SelectList label="Disable component css" options={this.examples.clearStyles} current={this.state.clearStyles} onOptionClick={this.changeClearStyles} />
                  <SelectList
                    label="Slide full height"
                    options={this.examples.slidesFullHeight}
                    current={this.state.slidesFullHeight}
                    onOptionClick={this.changeSlidesFullHeight}
                  />
                  <Input label="Background color" type="text" placeholder="CSS color property" onChange={this.changeBackgroundColor} value={this.state.backgroundColor} />

                  <Separator>OTHERS</Separator>
                  <SelectList
                    label="Youtube thumbnail quality"
                    options={this.examples.youtubeThumbQuality}
                    current={this.state.youtubeThumbQuality}
                    onOptionClick={this.changeYoutubeThumbQuality}
                  />
                </SelectBox>
              </div>
            </SelectWrap>
          </Wrap>
        </div>
      </div>
    );
  }
}

export default Slideshow;

const Wrap = styled.div`
  max-width: 850px;
  margin: 0 auto;

  .form-row {
    float: left;
    width: 100%;

    label {
      color: #617475;
      font-size: 11px;
      font-weight: 600;
      text-transform: uppercase;
      margin: 0 0 5px 0;
    }
    input {
      border-radius: 3px;
      border: 1px solid #b7b7b7;
      font-size: 13px;
      height: auto;
      padding: 8px;
    }
  }
`;

const SelectWrap = styled.div`
  position: fixed;
  display: table;
  right: 20px;
  top: 0;
  height: 100%;
`;

const SelectBox = styled.div`
  float: left;
  clear: both;
  height: 380px;
  width: 270px;
  padding: 20px;
  border-radius: 5px;
  overflow-y: scroll;
  overflow-x: hidden;
  border: 1px solid #ccc;
  background-color: #fdfdfd;
  box-shadow: 3px 6px 15px rgba(0, 0, 0, 0.04);
`;

const Separator = styled.div`
  float: left;
  width: 100%;
  color: #e28b00;
  font-size: 13px;
  font-weight: 600;
  padding: 0 0 5px 0;
  margin: 15px 0 0 0;
  text-transform: uppercase;
  border-bottom: 1px solid #1b1b1b;
`;

const ButtonsWrap = styled.div`
  float: left;
  width: 100%;
  margin: 0 0 15px 0;
`;

const ConfigButton = styled.div`
  float: left;
  width: 48%;
  padding: 10px 0;
  color: #8a8a8a;
  text-align: center;
  cursor: pointer;
  font-size: 13px;
  font-weight: 600;
  border-radius: 5px;
  text-transform: uppercase;
  box-sizing: border-box;
  border: 1px solid #ccc;
  background-color: #fdfdfd;
  box-shadow: 1px 3px 5px rgba(0, 0, 0, 0.02);
  transition-property: color, border-color, background-color;
  transition-duration: 0.3s;
  transition-timing-function: ease;

  &:first-child {
    margin-right: 4%;
  }
  &:hover {
    color: #459400;
  }
  &.current {
    color: #fff;
    border-color: #17bed0;
    background-color: #17bed0;
  }
`;
