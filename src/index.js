import React from "react";
import ReactDOM from "react-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import WebFont from "webfontloader";
import registerServiceWorker from "./utilities/registerServiceWorker";

import "normalize.css";
import "./styles/App.css";
import reducer from "./store/reducer";
import App from "./App";

const store = createStore(reducer);

WebFont.load({
  google: {
    families: ["Open Sans:300,400,700", "sans-serif"]
  }
});

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
