import React from "react";
import styled from "styled-components";

import Label from "./Label";

const Textarea = props => (
  <InputRow className={`form-row${props.error ? " error" : ""}${props.disabled ? " disabled" : ""}`}>
    {props.label && <Label label={props.label} error={props.error} />}

    <textarea
      ref={props.node}
      name={props.name}
      placeholder={props.placeholder}
      value={props.value}
      onChange={props.onChange}
      onBlur={props.onBlur}
      disabled={props.disabled}
      autoFocus={props.autofocus}
      dirname={props.dirname}
      maxLength={props.maxlength}
      readOnly={props.readonly}
      wrap={props.wrap}
      form={props.form}
      rows={props.rows}
      cols={props.cols}
    />
  </InputRow>
);

export default Textarea;

const InputRow = styled.div`
  position: relative;
  margin: 20px 0 0 0;

  &.error {
    label,
    textarea {
      color: #ff3300;
    }
    textarea,
    textarea:focus {
      border-color: #ff3300;
    }
  }

  textarea {
    display: block;
    width: 100%;
    height: 100px;
    min-width: 100%;
    max-height: 100px;
    min-height: 100px;
    padding: 10px;
    max-width: 100%;
    color: #112146;
    font-size: 15px;
    font-weight: 100;
    outline: none;
    margin: 5px 0 0 0;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #909090;
    background-color: #fff;
    transition: border-color 0.3s ease, background-color 0.3s ease;

    &:focus {
      border-color: #0087d2;
    }
    &[disabled] {
      color: #999;
      border-color: #ccc;
      background-color: #fbfbfb;
    }
    &::-webkit-input-placeholder {
      color: #888888;
      -webkit-text-fill-color: #888888;
    }
    &::-moz-placeholder {
      color: #888888;
      -webkit-text-fill-color: #888888;
    }
    &:-ms-input-placeholder {
      color: #888888;
      -webkit-text-fill-color: #888888;
    }
    &:-moz-placeholder {
      color: #888888;
      -webkit-text-fill-color: #888888;
    }
  }
`;
