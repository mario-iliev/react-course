import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const NotFound404 = props => (
  <div className="section">
    <div className="vertical-align">
      <Message>
        Sorry, <Link to="/">404 Page not found</Link> my friend.
      </Message>
    </div>
  </div>
);

export default NotFound404;

const Message = styled.div`
  text-align: center;
  font-size: 40px;
  font-weight: 100;
  letter-spacing: -1px;

  a {
    color: #17bed0;
    text-decoration: none;
    font-weight: 500;
    transition: color 0.3s ease;

    &:hover {
      color: #78d017;
    }
  }
`;
