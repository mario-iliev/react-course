import React from "react";
import styled from "styled-components";

import ListItem from "../components/content/ListItem";
import AtomLogoSVG from "../images/AtomLogoSVG";

const EditorSetupPage = props => (
  <Wrap>
    <AtomLogoSVG />
    <h1>Atom packages</h1>
    <div className="atom-packages-wrap">
      <strong>Development</strong>
      <ul>
        <ListItem link="https://atom.io/packages/emmet" text="emmet" target="_blank" />
        <ListItem link="https://atom.io/packages/prettier-atom" text="prettier-atom" target="_blank" />
        <ListItem link="https://atom.io/packages/atom-beautify" text="atom-beautify" target="_blank" />
        <ListItem link="https://atom.io/packages/linter-eslint" text="linter-eslint" target="_blank" />
        <ListItem link="https://atom.io/packages/language-babel" text="language-babel" target="_blank" />
        <ListItem link="https://atom.io/packages/atom-ternjs" text="atom-ternjs" target="_blank" />
      </ul>
      <strong>Atom UI</strong>
      <ul>
        <ListItem link="https://atom.io/packages/file-icons" text="file-icons" target="_blank" />
        <ListItem link="https://atom.io/themes/monokai" text="monokai" target="_blank" />
        <ListItem link="https://atom.io/packages/minimap" text="minimap" target="_blank" />
        <ListItem link="https://atom.io/packages/minimap-highlight-selected" text="minimap-highlight-selected" target="_blank" />
      </ul>
      <strong>Helpers</strong>
      <ul>
        <ListItem link="https://atom.io/packages/highlight-line" text="highlight-line" target="_blank" />
        <ListItem link="https://atom.io/packages/highlight-selected" text="highlight-selected" target="_blank" />
        <ListItem link="https://atom.io/packages/autoclose-html" text="autoclose-html" target="_blank" />
        <ListItem link="https://atom.io/packages/js-hyperclick" text="js-hyperclick" target="_blank" />
        <ListItem link="https://atom.io/packages/autocomplete-paths" text="autocomplete-paths" target="_blank" />
        <ListItem link="https://atom.io/packages/project-manager" text="project-manager" target="_blank" />
        <ListItem link="https://atom.io/packages/multi-cursor" text="multi-cursor" target="_blank" />
        <ListItem link="https://atom.io/packages/svg-preview" text="svg-preview" target="_blank" />
        <ListItem link="https://atom.io/packages/pigments" text="pigments" target="_blank" />
        <ListItem link="https://atom.io/packages/color-picker" text="color-picker" target="_blank" />
      </ul>
      <strong>Install everything (Terminal)</strong>
      <code className="custom">
        apm install emmet atom-beautify prettier-atom linter-eslint language-babel atom-ternjs react-es7-snippets file-icons monokai highlight-selected autoclose-html
        js-hyperclick autocomplete-paths project-manager minimap minimap-highlight-selected highlight-line multi-cursor svg-preview pigments color-picker
      </code>
    </div>
  </Wrap>
);

export default EditorSetupPage;

const Wrap = styled.div`
  text-align: center;
  overflow-y: scroll;
  height: 100%;

  .atom-packages-wrap {
    display: table;
    max-width: 480px;
    margin: 0 auto;
  }

  svg {
    width: 150px;
    display: block;
    margin: 40px auto 0 auto;
  }
  h1 {
    font-size: 35px;
    font-weight: 100;
    margin: 30px auto;
  }
  strong {
    font-size: 17px;
    float: left;
    font-weight: 500;
  }
  ul {
    float: left;
    width: 100%;
    padding: 0;
    margin: 10px 0 30px 0;
    list-style: none;

    li {
      float: left;
      clear: both;
      line-height: 15px;
      text-align: left;
      padding: 5px 0;

      a {
        display: block;
        color: #00a2b3;
        font-size: 15px;
        text-decoration: none;
        transition: color 0.3s ease;

        &:hover {
          color: #78d017;
        }
      }
    }
  }
`;
