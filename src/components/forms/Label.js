import React from "react";
import styled from "styled-components";

const FormLabel = props => (
  <Label>
    {props.label}
    {props.error && <span>{props.error}</span>}
  </Label>
);

export default FormLabel;

const Label = styled.label`
  display: block;
  color: #112146;
  width: 100%;
  text-align: left;
  font-size: 15px;
  font-weight: 600;
  transition: color 0.3s ease;

  span {
    float: right;
    font-size: 13px;
    margin: 3px 0 0 0;
    font-weight: 500;
  }
`;
