import React, { Component } from "react";
import styled, { keyframes } from "styled-components";
import { connect } from "react-redux";

import * as actionTypes from "../../store/actions";

class HamburgerButton extends Component {
  componentDidMount() {
    window.addEventListener("keyup", this.props.closeMenu, false);
  }

  componentWillUnmount() {
    window.removeEventListener("keyup", this.props.closeMenu, false);
  }

  render() {
    return (
      <Wrap className={`menu-button${this.props.opened ? " opened" : " closed"}`} onClick={this.props.toggleMenu}>
        <b className="_1" />
        <b className="_2" />
        <b className="_3" />
      </Wrap>
    );
  }
}

const mapStateToProps = state => ({
  opened: state.isMenuVisible
});

const mapDispatchToProps = dispatch => ({
  toggleMenu: () => dispatch({ type: actionTypes.TOGGLE_MENU }),
  closeMenu: () => dispatch({ type: actionTypes.CLOSE_MENU })
});

export default connect(mapStateToProps, mapDispatchToProps)(HamburgerButton);

const HamOpenLine1 = keyframes`
  0% {
    top: 9px;
    transform: rotate(43deg);
  }
  50% {
    top: 9px;
    transform: rotate(0deg);
  }
  100% {
    top: 0px;
  }
`;
const HamOpenLine2 = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`;
const HamOpenLine3 = keyframes`
  0% {
    top: 9px;
    transform: rotate(-43deg);
  }
  50% {
    top: 9px;
    transform: rotate(0deg);
  }
  100% {
    top: 18px;
  }
`;

const HamCloseLine1 = keyframes`
  0% {
    top: 0px;
  }
  50% {
    top: 9px;
    transform: rotate(0deg);
  }
  100% {
    top: 9px;
    transform: rotate(43deg);
  }
`;
const HamCloseLine2 = keyframes`
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
`;
const HamCloseLine3 = keyframes`
  0% {
    top: 18px;
  }
  50% {
    top: 9px;
    transform: rotate(0deg);
  }
  100% {
    top: 9px;
    transform: rotate(-43deg);
  }
`;

const Wrap = styled.div`
  position: fixed;
  cursor: pointer;
  width: 30px;
  height: 21px;
  top: 20px;
  right: 20px;
  z-index: 7;

  b {
    position: absolute;
    top: 0;
    right: 0;
    width: 100%;
    height: 3px;
    border-radius: 10px;
    background-color: #999;
    transition: width 0.2s ease;
    animation-fill-mode: forwards;
    animation-timing-function: linear;

    &._1 {
      top: 0;
      animation-duration: 0.3s;
    }
    &._2 {
      top: 9px;
      animation-duration: 0.15s;
    }
    &._3 {
      top: 18px;
      animation-duration: 0.3s;
    }
  }

  &.opened {
    b {
      &._1 {
        animation-name: ${HamCloseLine1};
      }
      &._2 {
        animation-name: ${HamCloseLine2};
      }
      &._3 {
        animation-name: ${HamCloseLine3};
      }
    }
  }

  &.closed {
    b {
      &._1 {
        animation-name: ${HamOpenLine1};
      }
      &._2 {
        animation-name: ${HamOpenLine2};
      }
      &._3 {
        animation-name: ${HamOpenLine3};
      }
    }
  }

  &:hover {
    &.closed {
      b {
        &._1 {
          width: 50%;
        }
        &._2 {
          width: 75%;
        }
      }
    }
  }

  &:hover,
  &.opened {
    b {
      background-color: #17bed0;
    }
  }
`;
