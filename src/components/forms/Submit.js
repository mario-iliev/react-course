import React from "react";
import styled from "styled-components";

const Submit = props => (
  <InputRow className={`form-row${props.loading ? " loading" : ""}`}>
    <input type="submit" value={props.value} disabled={props.disabled} />
  </InputRow>
);

export default Submit;

const InputRow = styled.div`
  position: relative;
  margin: 20px 0 0 0;

  input {
    display: block;
    width: 100%;
    height: auto;
    max-width: 200px;
    color: rgb(255, 255, 255);
    font-size: 15px;
    font-weight: 600;
    border-width: 0px;
    margin: 0px auto;
    padding: 13px 0px;
    border-radius: 4px;
    background-color: #112146;
    cursor: pointer;
    text-transform: uppercase;
    transition: background-color 0.3s ease;

    &:hover {
      background-color: #4e68a5;
    }
    &[disabled] {
      columns: #fff;
      cursor: default;
      background-color: #cecece;
    }
  }
`;
