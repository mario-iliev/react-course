import React from "react";
import styled from "styled-components";

const ReadMore = props => (
  <Anchor className="read-more" href={props.link} target="_blank" rel="noopener noreferrer">
    {props.text || "Read more"}
  </Anchor>
);

export default ReadMore;

const Anchor = styled.a`
  display: inline-block;
  font-size: 15px;
  color: #17bed0;
  font-weight: 600;
  text-transform: uppercase;
  text-decoration: none;
  margin: 15px auto 0 auto;
  transition: color 0.3s ease;

  &:hover {
    color: #78d017;
  }
`;
