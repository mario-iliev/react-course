import React from "react";
import styled from "styled-components";

const SuccessMessage = props => (
  <Wrap className="form-success" onClick={props.onClick}>
    {props.children}
    <div className="message">{props.message}</div>
  </Wrap>
);

export default SuccessMessage;

const Wrap = styled.div`
  cursor: pointer;
  color: #6da700;
  padding: 10px;
  margin: 0 0 20px 0;
  border-radius: 4px;
  text-align: center;
  border: 1px solid #8eda00;
  background-color: #fefffc;
`;
