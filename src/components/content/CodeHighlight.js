import React from "react";
import styled from "styled-components";

const CodeHighlight = props => (
  <Pre>
    <code>{props.children}</code>
  </Pre>
);

export default CodeHighlight;

const Pre = styled.pre`
  border-radius: 15px;
  display: table;
  font-size: 14px;
  text-align: left;
  padding: 0 15px;
  margin: 30px auto;
  color: rgb(248, 248, 242);
  background: rgb(35, 36, 31);
`;
