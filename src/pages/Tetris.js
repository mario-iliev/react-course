import React, { Component } from "react";
import styled from "styled-components";

import api from "../utilities/api";
import backgroundImage from "../images/snake.svg";

// TODO: This is under development, don't even look at it

class Tetris extends Component {
  constructor(props) {
    super(props);

    this.state = {
      score: 0,
      gameOver: false,
      paused: false
    };
    this.config = {
      blockSize: 20,
      boardWidth: 240,
      boardHeight: 400,
      dropSpeed: 1000
    };
    this.initialState = JSON.parse(JSON.stringify(this.state));
    this.board = api.createArrayMatrix(this.config.boardWidth / this.config.blockSize, this.config.boardHeight / this.config.blockSize);
    this.shapes = {
      T: [0, 0, 0, 1, 1, 1, 0, 1, 0]
    };
  }

  initAnimationFrame = (time = 0) => {
    this.animationFrame = requestAnimationFrame(this.initAnimationFrame);

    if (time - this.lastUpdateTime >= this.config.dropSpeed) {
      this.lastUpdateTime = time;

      // Do something
    }
  };

  startGame = () => {
    this.lastUpdateTime = 0;

    this.initAnimationFrame();

    // Do something
  };

  endGame = () => {
    cancelAnimationFrame(this.animationFrame);
    this.setHighScore();
    this.setState({ gameOver: true });
  };

  pauseGame = () => {
    cancelAnimationFrame(this.animationFrame);
    this.setState({ paused: true });
  };

  resumeGame = () => {
    if (!this.state.gameOver) {
      this.initAnimationFrame();
      this.setState({ paused: false });
    }
  };

  setHighScore = () => {
    const highScore = api.localStorage.get("tetrisHighScore");

    if (!highScore || highScore < this.state.score) {
      api.localStorage.set("tetrisHighScore", this.state.score);
    }
  };

  onKeyPress = e => {
    let restart = false;
    let pause = false;
    let key = e.keyCode;

    if (key === 27) {
      pause = true;
    } else if (key === 13 || key === 32) {
      restart = true;
    }

    if (pause && !this.state.gameOver) {
      if (this.state.paused) {
        this.resumeGame();
      } else {
        this.pauseGame();
      }
    } else if (restart && this.state.gameOver) {
      this.startGame();
    }
  };

  componentDidMount() {
    this.startGame();

    window.addEventListener("keydown", this.onKeyPress, false);
  }

  componentWillUnmount() {
    cancelAnimationFrame(this.animationFrame);

    window.removeEventListener("keydown", this.onKeyPress, false);
  }

  render() {
    return (
      <div className="section">
        <div className="vertical-align">
          <Wrap>
            <Board config={this.config} />
          </Wrap>
        </div>
      </div>
    );
  }
}

export default Tetris;

const Wrap = styled.div`
  display: table;
  margin: 0 auto;
`;

const Board = styled.div`
  position: relative;
  overflow: hidden;
  margin: 0 auto;
  background-color: #a5bdb6;
  border: 1px solid #65756b;
  box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.05);
  background-image: url(${backgroundImage});
  background-repeat: repeat;
  background-size: ${props => props.config.blockSize}px;
  width: ${props => props.config.boardWidth}px;
  height: ${props => props.config.boardHeight}px;

  .overlay {
    position: absolute;
    display: table;
    width: 100%;
    height: 100%;
    left: 0;
    z-index: 2;
    color: #a5c5b4;
    font-weight: 600;
    font-size: 19px;
    cursor: pointer;
    text-align: center;
    text-transform: uppercase;
    background-color: rgba(37, 88, 75, 0.7);
  }
`;
