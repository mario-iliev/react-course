import React from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import styled from "styled-components";

import routes from "../../routes/routes";
import * as actionTypes from "../../store/actions";

const Menu = props => {
  const items = routes.map(
    (item, i) =>
      item.inMainMenu && (
        <li key={`main-menu-${i}`}>
          {item.disabledInMenu ? (
            <span className="inactive">{item.menuName}</span>
          ) : (
            <NavLink exact to={item.url} activeClassName="current" onClick={props.closeMenu}>
              {item.menuName}
            </NavLink>
          )}
        </li>
      )
  );

  return (
    <Wrap className={`menu-wrap${props.opened ? " is-opened" : ""}`}>
      <ul>{items}</ul>
      <div className="menu-background" onClick={props.closeMenu} />
    </Wrap>
  );
};

const mapStateToProps = state => ({
  opened: state.isMenuVisible
});

const mapDispatchToProps = dispatch => ({
  closeMenu: () => dispatch({ type: actionTypes.CLOSE_MENU })
});

export default connect(mapStateToProps, mapDispatchToProps)(Menu);

const Wrap = styled.div`
  position: fixed;
  overflow: hidden;
  width: 100%;
  height: 100%;
  top: -100%;
  z-index: 6;
  left: 0;
  opacity: 0;

  transition: opacity 0.2s ease;

  &.is-opened {
    top: 0;
    opacity: 1;

    ul {
      opacity: 1;
      margin-right: 0;
    }
  }

  .menu-background {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background-color: rgba(0, 0, 0, 0.35);
  }

  ul {
    float: right;
    width: 320px;
    height: 100%;
    opacity: 0;
    margin: 0 -500px 0 0;
    padding: 55px 0 5px 0;
    background: #fff;
    z-index: 1;
    list-style: none;
    position: relative;
    box-sizing: border-box;
    transition: opacity 0.5s ease, margin-right 0.5s ease;
    box-shadow: 0px 18px 110px rgba(0, 0, 0, 0.4);
  }

  li {
    float: left;
    text-align: right;
    font-size: 14px;
    width: 100%;
    padding: 10px 20px 10px 0px;
    border-top: 1px solid #dedede;
    box-sizing: border-box;

    &:first-child {
      border: 0;
    }
  }

  a {
    color: #2c2c2c;
    text-decoration: none;
    transition: color 0.3s ease;

    &:hover {
      color: #17bed0;
    }

    &.current {
      cursor: default;
      color: #459400;
    }
  }

  .inactive {
    cursor: default;
    color: #ccc;
  }
`;
