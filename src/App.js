import React, { Fragment } from "react";
import { Route, Switch } from "react-router-dom";

import routes from "./routes/routes";

const App = () => (
  <Switch>
    {routes.map((route, i) => (
      <Route
        key={i}
        path={route.url}
        exact={route.urlExactMatch}
        render={() => (
          <Fragment>
            {route.head}
            <route.layout>
              <route.component />
            </route.layout>
          </Fragment>
        )}
      />
    ))}
  </Switch>
);

export default App;
