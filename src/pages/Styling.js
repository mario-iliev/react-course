import React from "react";

import SectionScroller from "../components/content/section-scroller/Basic";
import CodeHighlight from "../components/content/CodeHighlight";
import ReadMore from "../components/content/ReadMore";

const PageStyling = props => {
  const content = [
    {
      section: [
        <div className="content" key="styled-comp">
          <div className="underline">Styled Components</div>
          <CodeHighlight>{`
const Wrap = styled.div'
  width: 100px;
  height: ${props => props.position * 20};

  &:hover {
    background-color: #17bed0;
  }
';
          `}</CodeHighlight>
          <ReadMore link="https://www.styled-components.com/docs/basics" />
        </div>
      ]
    },
    {
      section: [
        <div className="content" key="chokidar">
          <div className="underline">React & SASS (Chokidar)</div>
          <CodeHighlight>{`
npm install --save node-sass-chokidar
npm install --save npm-run-all
        `}</CodeHighlight>
          <CodeHighlight>{`
"scripts": {
  "build-css": "node-sass-chokidar src/ -o src/",
  "watch-css": "npm run build-css && node-sass-chokidar src/ -o src/ --watch --recursive",
  "start-js": "react-scripts start",
  "start": "npm-run-all -p watch-css start-js",
  "build-js": "react-scripts build",
  "build": "npm-run-all build-css build-js",
  "test": "react-scripts test --env=jsdom",
  "eject": "react-scripts eject"
}
          `}</CodeHighlight>
          <ReadMore link="https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#adding-a-css-preprocessor-sass-less-etc" />
        </div>
      ]
    }
  ];

  return <SectionScroller content={content} />;
};
export default PageStyling;
