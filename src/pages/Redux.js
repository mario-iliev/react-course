import React from "react";
import styled from "styled-components";

import ReadMore from "../components/content/ReadMore";
import CodeHighlight from "../components/content/CodeHighlight";

const ReduxExample = props => (
  <Wrap>
    <h1>This is a simple example of creating Redux state "store" and using it.</h1>
    <p>
      You can learn Redux from the <ReadMore link="https://redux.js.org/introduction" text="Redux site" /> Also check the Dan Abramov's{" "}
      <ReadMore link="https://egghead.io/lessons/react-redux-the-single-immutable-state-tree" text="video tutorial" />
    </p>
    <h2>Creating the state "store"</h2>
    <CodeHighlight>
      {`
import { createStore } from "redux";
import { Provider } from "react-redux";

// Create our initial state
const initialState = {
  counter: 0
};

// Create the reducer function. Usually the reducer and the initial state live together in a reducer.js file
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "INCREMENT":
      return {
        counter: state.counter + 1
      };
    case "DECREMENT":
      return {
        counter: state.counter - 1
      };
    default:
      return state;
  }
};

// Create the Redux store
const store = createStore(reducer);

// 1. Wrap the App component with the "Provider" imported from "react-redux"
// 2. Pass the Redux store (the app state) to the Provider
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
    `}
    </CodeHighlight>
    <h2>Using and updating the Redux state</h2>

    <CodeHighlight>
      {`
// Now in any component nested in our App, we can now display and change the global Redux state.

// Import "connect" with which we'll connect with the Redux state
import { connect } from "react-redux";

// Simple example component
const CounterComponent = props => (
  <div>
    <!-- Display the current count number. It's passed from the global state trough the "mapStateToProps" showed below -->
    <div>{props.count}</div>

    <!-- The click functions which affect the global state are passed from the "mapDispatchToProps" showe below -->
    <button onClick={props.increment}>Increment the counter</button>
    <button onClick={props.decrement}>Decrement the counter</button>
  </div>
);

// Recieve a value from the global Redux state
const mapStateToProps = state => ({
  count: state.counter
});

// Dispatch actions
const mapDispatchToProps = dispatch => ({
  increment: () => dispatch({ type: "INCREMENT" }),
  decrement: () => dispatch({ type: "DECREMENT" })
});

// Either one of the "mapStateToProps" and "mapDispatchToProps" could not be used if we don't need them
export default connect(mapStateToProps, mapDispatchToProps)(CounterComponent);
      `}
    </CodeHighlight>
  </Wrap>
);

export default ReduxExample;

const Wrap = styled.div`
  text-align: center;
  overflow-y: scroll;
  height: 100%;

  h1 {
    font-size: 32px;
    font-weight: 400;
    margin: 20px auto 0 auto;
  }

  h2 {
    font-weight: 500;
  }
`;
