import React from "react";
import styled from "styled-components";
import animations from "../../styles/animations";

const Loading = props => <Spinner size={props.size || 50} borderWidth={props.borderWidth} />;

export default Loading;

const Spinner = styled.div`
  width: ${props => props.size}px;
  height: ${props => props.size}px;
  margin: -${props => props.size / 2}px 0px 0px -${props => props.size / 2}px;
  border: ${props => props.borderWidth || Math.floor(props.size / 13)}px solid #e8e8e8;
  border-left-color: #17bed0;
  position: absolute;
  top: 50%;
  left: 50%;
  border-radius: 100%;
  transform: translateZ(0);
  box-sizing: border-box;
  animation: ${animations.spinInfinite} 0.7s infinite linear;
`;
