import React, { Component } from "react";

import api from "../utilities/api";
import Form from "../components/forms/Form";
import Input from "../components/forms/Input";
import Radio from "../components/forms/Radio";
import Submit from "../components/forms/Submit";
import Textarea from "../components/forms/Textarea";
import SuccessMessage from "../components/forms/SuccessMessage";

class FormContact extends Component {
  constructor() {
    super();

    this.state = {
      error: false,
      success: false,
      loading: false,
      disabled: false,
      email: {
        value: "",
        error: false,
        focus: false
      },
      name: {
        value: "",
        error: false,
        focus: false
      },
      message: {
        value: "",
        error: false,
        focus: false
      },
      radioButtons: {
        google: {
          checked: true
        },
        facebook: {
          checked: false
        },
        twitter: {
          checked: false
        }
      }
    };

    this.myInitialState = JSON.parse(JSON.stringify(this.state));
    this.radioButtons = [
      {
        value: "google",
        text: "Google"
      },
      {
        value: "facebook",
        text: "Facebook"
      },
      {
        value: "twitter",
        text: "Twitter"
      }
    ];
    this.nodes = {};
  }

  onInputChange = e => {
    if (!this.state.loading) {
      const newState = JSON.parse(JSON.stringify(this.state));

      newState[e.target.name].value = e.target.value;

      for (var i in newState) {
        if (newState[i].error) {
          newState[i].error = false;
        }

        if (newState[i].focus) {
          newState[i].focus = false;
        }
      }

      this.setState(newState);
    }
  };

  onRadioChange = e => {
    const newState = JSON.parse(JSON.stringify(this.state));

    for (let i in newState.radioButtons) {
      newState.radioButtons[i].checked = i === e.target.value ? true : false;
    }

    this.setState(newState);
  };

  onNodeLoaded = node => {
    if (node && node.name) {
      this.nodes[node.name] = node;
    }
  };

  onSubmit = e => {
    const newState = JSON.parse(JSON.stringify(this.state));

    e.preventDefault();

    if (!api.isEmailValid(this.state.email.value)) {
      newState.email.error = "Please provide a valid email address";
      newState.email.focus = true;
    } else if (this.state.name.value.length <= 3) {
      newState.name.error = "Name must be at least 3 symbols";
      newState.name.focus = true;
    } else if (this.state.message.value.length <= 10) {
      newState.message.error = "Message must be at least 10 symbols";
      newState.message.focus = true;
    } else {
      // Simulate AJAX call with success callback
      setTimeout(() => {
        this.setState({
          loading: false,
          success: <SuccessMessage message="Your message is sent!" onClick={this.resetForm} />
        });
      }, 3000);

      newState.loading = true;
      newState.disabled = true;
    }

    this.setState(newState);
  };

  resetForm = () => {
    this.setState(JSON.parse(JSON.stringify(this.myInitialState)));
  };

  render() {
    for (var i in this.state) {
      if (this.state[i].focus) {
        this.nodes[i].focus();

        break;
      }
    }

    return (
      <div className="section">
        <div className="vertical-align">
          <Form onSubmit={this.onSubmit} error={this.state.error} success={this.state.success}>
            <Input
              label="Email"
              type="email"
              name="email"
              placeholder="Please enter your email"
              autofocus={true}
              onChange={this.onInputChange}
              value={this.state.email.value}
              error={this.state.email.error}
              disabled={this.state.disabled}
              node={this.onNodeLoaded}
            />
            <Input
              label="Name"
              type="text"
              name="name"
              placeholder="Please enter your first name"
              onChange={this.onInputChange}
              value={this.state.name.value}
              error={this.state.name.error}
              disabled={this.state.disabled}
              node={this.onNodeLoaded}
            />
            <Radio label="Reffered by" buttons={this.radioButtons} checked={this.state.radioButtons} onChange={this.onRadioChange} disabled={this.state.disabled} />
            <Textarea
              label="Message"
              name="message"
              placeholder="Please enter your message"
              onChange={this.onInputChange}
              value={this.state.message.value}
              error={this.state.message.error}
              disabled={this.state.disabled}
              node={this.onNodeLoaded}
            />
            <Submit value="Submit" loading={this.state.loading} disabled={this.state.disabled} />
          </Form>
        </div>
      </div>
    );
  }
}

export default FormContact;
