import React from "react";

import SectionScroller from "../components/content/section-scroller/Basic";
import ReadMore from "../components/content/ReadMore";
import CodeHighlight from "../components/content/CodeHighlight";

import ByFacebookGIF from "../images/gif/by-facebook.gif";
import OneWayBindingGIF from "../images/gif/one-way-binding.gif";
import CoolGIF from "../images/gif/cool.gif";
import VirtualDomGIF from "../images/gif/virtual-dom.gif";
import UsedByGIF from "../images/gif/used-by-everyone.gif";
import StatePropsGIF from "../images/gif/state-props.gif";
import ThankYouGIF from "../images/gif/thank-you-1.gif";

const WhatIsReact = props => {
  const content = [
    {
      section: [
        <div className="content" key="is-lib">
          <div className="underline">
            <strong>React</strong> is developed by Facebook. It <strong>is a</strong> Javascript <strong>library</strong>! Or is it a <strong>framework</strong>...
          </div>
          <img className="gif-img" src={ByFacebookGIF} alt="" />
          <ReadMore link="https://reactjs.org/tutorial/tutorial.html#what-is-react" />
        </div>
      ]
    },
    {
      anchor: "react-is-famous",
      section: [
        <div className="content" key="its-famous">
          <h1 className="underline">Who's using React?</h1>
          <p>Facebook, Instagram, Airbnb, Skype, Tesla, SoundCloud, Wix, FFW...</p>
          <span className="grey-quote">"Everybody"</span>
          <img className="gif-img" src={UsedByGIF} alt="" />
          <ReadMore link="https://github.com/facebook/react/wiki/Sites-Using-React" />
        </div>
      ]
    },
    {
      anchor: "why-react",
      section: [
        <div className="content" key="why-react">
          <h1 className="underline">Why are they using it?</h1>
          <p>Declarative, Reusable, Reactive!</p>
          <img className="gif-img" src={CoolGIF} alt="" />
          <ReadMore link="https://stories.jotform.com/7-reasons-why-you-should-use-react-ad420c634247" />
        </div>
      ]
    },
    {
      anchor: "data-flow",
      section: [
        <div className="content" key="one-way">
          <h1 className="underline">One-way data flow</h1>
          <p>
            Properties (commonly, props) are passed to a component from the parent component. Components receive props as a single set of immutable values (a JavaScript
            object). Whenever any prop value changes, the component's render function is called allowing the component to display the change.
          </p>
          <img className="gif-img" src={OneWayBindingGIF} alt="" />
          <ReadMore link="https://medium.com/@lizdenhup/understanding-unidirectional-data-flow-in-react-3e3524c09d8e" />
        </div>
      ]
    },
    {
      anchor: "virtual-dom",
      section: [
        <div className="content" key="virtual-dom">
          <h1 className="underline">Virtual DOM</h1>
          <p>React creates an in-memory data structure cache, computes the resulting differences, and then updates the browser's displayed DOM efficiently.</p>
          <img className="gif-img" src={VirtualDomGIF} alt="" />
          <ReadMore link="https://hackernoon.com/virtual-dom-in-reactjs-43a3fdb1d130" />
        </div>
      ]
    },
    {
      anchor: "jsx",
      section: [
        <div className="content" key="jsx-is-cool">
          <h1 className="underline">JSX</h1>
          <p>JavaScript XML (JSX) is an extension to the JavaScript language syntax. Nesting, attributes and expressions.</p>
          <CodeHighlight>
            {`
return (
  <div className="container">
    <span onClick={ this.myClickFunction }>
      Scroll Top
    </span>
  </div>
);
            `}
          </CodeHighlight>
          <ReadMore link="https://reactjs.org/docs/introducing-jsx.html" />
          <span className="between-links">and</span>
          <ReadMore link="https://reactjs.org/docs/jsx-in-depth.html" text="even more" />
        </div>
      ]
    },
    {
      anchor: "get-started",
      section: [
        <div className="content" key="get-started">
          <h1 className="underline">Create React App</h1>
          <CodeHighlight>
            {`
npm install -g create-react-app
create-react-app your-project-name
cd your-project-name
npm start
            `}
          </CodeHighlight>
          <ReadMore link="https://reactjs.org/docs/add-react-to-a-new-app.html" />
        </div>
      ]
    },
    {
      anchor: "state-props",
      section: [
        <div className="content" key="state-props">
          <h1 className="underline">State &amp; Props</h1>
          <p>Props are used to pass data from parent to child or by the component itself. They are immutable and thus will not be changed.</p>
          <img className="gif-img" src={StatePropsGIF} alt="" />
          <ReadMore link="https://hackernoon.com/understanding-state-and-props-in-react-94bc09232b9c" />
        </div>
      ]
    },
    {
      anchor: "thank-you",
      section: [
        <div className="content" key="thank-you">
          <h1 className="underline">Thank you!</h1>
          <img className="gif-img" src={ThankYouGIF} alt="" />
        </div>
      ]
    }
  ];

  return <SectionScroller content={content} />;
};

export default WhatIsReact;
