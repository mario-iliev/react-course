import { keyframes } from "styled-components";

const animations = {
  fadeIn: keyframes`
    0% { opacity: 0; }
    100% { opacity: 1; }
  `,
  fadeOut: keyframes`
    0% { opacity: 1; }
    100% { opacity: 0; }
  `,
  fadeInFromHalf: keyframes`
    0% { opacity: 0.5; }
    100% { opacity: 1; }
  `,
  slideHideToLeft: keyframes`
    0% { left: 0; }
    100% { left: -100%; }
  `,
  slidePartialHideToLeft: keyframes`
    0% { left: 0; }
    100% { left: -70%; }
  `,
  slideHideToRight: keyframes`
    0% { left: 0;}
    100% { left: 100%; }
  `,
  slidePartialHideToRight: keyframes`
    0% { left: 0;}
    100% { left: 70%; }
  `,
  slideHideToTop: keyframes`
    0% { top: 0;}
    100% { top: -100%; }
  `,
  slideHideToBottom: keyframes`
    0% { top: 0;}
    100% { top: 100%; }
  `,
  slideShowFromLeft: keyframes`
    0% { left: 100%; }
    100% { left: 0; }
  `,
  slideShowFromRight: keyframes`
    0% { left: -100%; }
    100% { left: 0; }
  `,
  slideShowFromTop: keyframes`
    0% { top: -100%; }
    100% { top: 0; }
  `,
  slideShowFromBottom: keyframes`
    0% { top: 100%; }
    100% { top: 0; }
  `,
  zoomOutFading: keyframes`
    0% { transform: scale(1); opacity: 1; }
    100% { transform: scale(0.2); opacity: 0; }
  `,
  zoomInFading: keyframes`
    0% { transform: scale(1); opacity: 1; }
    100% { transform: scale(2.5); opacity: 0; }
  `,
  spinInfinite: keyframes`
    0% { transform: rotate(0deg); }
    100% { transform: rotate(359deg); }
  `,
  popIn: keyframes`
    0% { transform: scale(0.5); }
    100% { transform: scale(1); }
  `,
  popInBounce: keyframes`
    0% { transform: scale(0.5); }
    40% { transform: scale(1.2); }
    100% { transform: scale(1); }
  `
};

export default animations;
