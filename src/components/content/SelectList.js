import React, { Component } from "react";
import styled from "styled-components";

class SelectList extends Component {
  state = {
    opened: false
  };

  onMouseEnter = () => {
    this.setState({
      opened: true
    });
  };

  onMouseLeave = () => {
    this.setState({
      opened: false
    });
  };

  onOptionClick = value => {
    this.setState({
      opened: false
    });

    this.props.onOptionClick(value);
  };

  render() {
    const currentOption = this.props.options.map(option => option.value === this.props.current && option.text);
    const options = this.props.options.map(
      option =>
        option.value !== this.props.current && (
          <div className="option" onClick={() => this.onOptionClick(option.value)} key={`sel-${option.value}`}>
            {option.text}
          </div>
        )
    );

    return (
      <Wrap className={this.state.opened ? "opened" : ""}>
        <div className="select-label">{this.props.label}</div>
        <div className="select-wrap" onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}>
          <div className="selected">{currentOption}</div>
          <div className="select-list">{options}</div>
        </div>
      </Wrap>
    );
  }
}

export default SelectList;

const Wrap = styled.div`
  float: left;
  width: 100%;
  clear: both;
  margin-top: 15px;

  &:first-child {
    margin-top: 0;
  }

  &.opened {
    .select-wrap {
      .selected {
        color: #9c9c9c;
        cursor: default;
      }

      .select-list {
        left: -1px;
        top: 29px;
        opacity: 1;
        height: auto;
        overflow: visible;
      }
    }
  }

  .select-label {
    color: #617475;
    font-size: 11px;
    font-weight: 600;
    text-transform: uppercase;
    margin: 0 0 5px 0;
  }

  .select-wrap {
    border-radius: 3px;
    border: 1px solid #b7b7b7;
    position: relative;
    font-size: 13px;
    padding: 8px;
    background-color: #fff;

    &:hover {
      .selected {
        &::before {
          transform: rotate(0);
        }
        &::after {
          transform: rotate(0);
        }
      }
    }

    .selected {
      position: relative;
      cursor: pointer;
      color: #5a5a5a;

      &::before,
      &::after {
        position: absolute;
        content: "";
        width: 5px;
        height: 1px;
        top: 7px;
        right: 0;
        background-color: #757575;
        transition: transform 0.3s ease;
      }

      &::before {
        transform: rotate(35deg);
        right: 4px;
      }
      &::after {
        transform: rotate(-35deg);
      }
    }

    .select-list {
      position: absolute;
      cursor: pointer;
      top: 19px;
      left: 999999px;
      opacity: 0;
      height: 0;
      overflow: hidden;
      width: 100%;
      z-index: 1;
      border-radius: 0px 0px 3px 3px;
      border: 1px solid #b7b7b7;
      border-top-color: #969696;
      background-color: #fff;
      box-shadow: 0px 3px 1px rgba(0, 0, 0, 0.05);
      transition-property: opacity, top;
      transition-duration: 0.2s;
      transition-timing-function: ease;

      .option {
        padding: 8px;
        border-top: 1px solid #eaeaea;
        transition: color 0.3s ease;

        &:first-child {
          border-top: 0;
        }
        &:hover {
          color: #459400;
        }
        &.current {
          cursor: default;
          color: #17bed0;
        }
      }
    }
  }
`;
