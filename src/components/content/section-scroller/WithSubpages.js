import React, { Component } from "react";
import styled from "styled-components";

import api from "../../../utilities/api";

/*
  Accepts "content" prop type Array of Objects containing "section" key with an Array value.
  The "anchor" key is not required. A number will be used if "anchor" not provided.
  The "visited" function is not required. If available, this function will be called once when the section is seen from the user.
  This component expects "subpageOpened" prop to determine if a subpage should be opened. that means that open/close handling is done in the parent component.
  Example "content" structure passed to this component:
  const passThis = [
    {section: [<div>Section one</div>], anchor: "intro-section", visited: () =>{}},
    {section: [<div>Section two</div>], anchor: "second-section", visited: () =>{}, subpages: [ { content: [<div>Subpage</div>, anchor: "whatever"] } ]}
  ];
*/

class SectionScroller extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeSection: 0,
      changeInProgress: false
    };
    this.lastScrollTimeStamp = 0;
    this.resizeTimeout = false;
    this.componentEvents = {
      wheel: this.onScrollOrKey,
      keydown: this.onScrollOrKey,
      hashchange: this.urlHashChange,
      resize: this.onResize
    };
    this.sections = {};

    if (window.location.hash) {
      this.skipSectionVisitedFunction = true;
    }

    if (window.pageYOffset > 0 && this.state.activeSection === 0) {
      window.scrollTo(0, 0);
    }

    this.props.content.forEach((item, index) => {
      this.sections[index] = {
        seen: false
      };
    });
  }

  changeSection = (sectionID, initCheckSeenFunction) => {
    if (sectionID !== this.state.activeSection && !this.state.changeInProgress) {
      this.setState({
        activeSection: sectionID,
        changeInProgress: true
      });

      api.scrollTo(sectionID * window.innerHeight, 600, "easeOutQuad", () => this.setState({ changeInProgress: false }));

      if (sectionID === 0) {
        window.history.pushState(null, null, " ");
      } else {
        window.location.hash = this.props.content[sectionID].anchor || sectionID + 1;
      }
    }
  };

  onScrollOrKey = e => {
    const timeStamp = Math.round(e.timeStamp);

    if (this.state.changeInProgress === false) {
      if (timeStamp - this.lastScrollTimeStamp > 50) {
        if ((e.deltaY && e.deltaY < 0) || (e.keyCode && (e.keyCode === 38 || e.keyCode === 33))) {
          if (this.state.activeSection > 0) {
            this.changeSection(this.state.activeSection - 1);
          }
        } else if ((e.deltaY && e.deltaY > 0) || (e.keyCode && (e.keyCode === 40 || e.keyCode === 34))) {
          if (this.state.activeSection < this.props.content.length - 1) {
            this.changeSection(this.state.activeSection + 1);
          }
        }
      }
    }

    this.lastScrollTimeStamp = timeStamp;
  };

  urlHashChange = () => {
    let hash = window.location.hash.replace("#", "");
    let activeSection = false;

    for (let i in this.props.content) {
      if (this.props.content[i].anchor === hash) {
        activeSection = Number(i);

        break;
      }
    }

    if (!activeSection) {
      activeSection = Number(hash);

      if (isNaN(activeSection) || activeSection > this.props.content.length) {
        window.history.pushState(null, null, " ");

        api.scrollTo(0, 600, "easeOutQuad", () => this.setState({ changeInProgress: false }));

        this.setState({ activeSection: 0 });

        return;
      }

      if (activeSection > 0) {
        activeSection -= 1;
      }
    }

    if (activeSection !== this.state.activeSection) {
      this.changeSection(activeSection, false);
    }
  };

  checkSectionVisitedFunction = () => {
    if (!this.sections[this.state.activeSection].seen) {
      this.sections[this.state.activeSection].seen = true;

      if (typeof this.props.content[this.state.activeSection].visited === "function") {
        this.props.content[this.state.activeSection].visited();
      }
    }
  };

  onResize = e => {
    clearTimeout(this.resizeTimeout);

    this.resizeTimeout = setTimeout(() => {
      if (window.pageYOffset > 0 && window.innerHeight * this.state.activeSection !== window.pageYOffset) {
        window.scrollTo(0, window.innerHeight * this.state.activeSection);
      }
    }, 50);
  };

  componentDidMount() {
    if (window.location.hash) {
      this.urlHashChange();
    }

    for (let event in this.componentEvents) {
      window.addEventListener(event, this.componentEvents[event], false);
    }
  }

  componentWillUnmount() {
    for (let event in this.componentEvents) {
      window.removeEventListener(event, this.componentEvents[event], false);
    }
  }

  render() {
    const pages = [];
    const navigation = [];

    if (this.skipSectionVisitedFunction) {
      this.skipSectionVisitedFunction = false;
    } else {
      this.checkSectionVisitedFunction();
    }

    this.props.content.forEach((item, index) => {
      const currentClass = this.state.activeSection === index ? " current" : "";
      const seenClass = this.sections[index].seen ? " seen" : "";
      const subpages = [];

      if (item.subpages) {
        item.subpages.forEach((subpage, subindex) => {
          const activeClass = this.props.subpageOpened && this.props.subpageOpened === subpage.anchor ? " opened" : "";

          subpages.push(
            <SubPage className={`subpage${activeClass}`} key={`sub-${index}-${subindex}`}>
              {subpage.content}
            </SubPage>
          );
        });
      }

      pages.push(
        <Section className={`full-section${seenClass + currentClass}`} position={`${100 * index}%`} key={`sec-${index}`}>
          <div className="vertical-align">{item.section}</div>
          {subpages}
        </Section>
      );

      if (this.props.navigation !== false) {
        navigation.push(<div className={`section-nav${seenClass + currentClass}`} onClick={() => this.changeSection(index)} key={`side-nav-${index}`} />);
      }
    });

    return (
      <Wrap>
        <div className="section-wrap">{pages}</div>
        <div className="side-nav-wrap">
          <div className="vertical-align">{navigation}</div>
        </div>
      </Wrap>
    );
  }
}

export default SectionScroller;

const Wrap = styled.main`
  section {
    display: table;
    position: absolute;
    left: 0;
    width: 100%;
    height: 100%;
    opacity: 0.3;
    transition: opacity 0.6s ease;

    &.current {
      opacity: 1;
    }
    &:nth-child(even) {
      background-color: #f7f7f7;
    }
  }

  .side-nav-wrap {
    position: fixed;
    display: table;
    top: 0;
    right: 30px;
    height: 100%;

    .section-nav {
      cursor: pointer;
      width: 15px;
      height: 15px;
      border-radius: 2px;
      margin-top: 10px;
      background-color: #999;
      transition: background-color 0.3s ease;

      &.current {
        background-color: #78d017;
      }
      &:hover {
        background-color: #17bed0;
      }
      &:first-child {
        margin-top: 0;
      }
    }
  }
`;

const Section = styled.section`
  top: ${props => props.position};
`;

const SubPage = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 100%;
  z-index: 1000;
  background: #fff;
  transition: left 0.8s ease;

  &.opened {
    left: 0;
  }
`;
