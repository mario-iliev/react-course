import React from "react";
import Loadable from "react-loadable";
import { Helmet } from "react-helmet";

// Layouts
import MainLayout from "../components/layouts/MainLayout";

// Loaders
import Loading from "../components/content/Loading";

const globalConfig = {
  title: "React course and presentation",
  description: "React presentation and examples for discussion.",
  layout: MainLayout
};

const routes = [
  {
    url: "/",
    urlExactMatch: true,
    inMainMenu: true,
    disabledInMenu: false,
    menuName: "Home",
    component: Loadable({
      loader: () => import("../pages/Home"),
      loading: Loading
    }),
    layout: MainLayout,
    head: {
      title: globalConfig.title,
      description: globalConfig.description
    }
  },
  {
    url: "/what-is-react",
    urlExactMatch: true,
    inMainMenu: true,
    disabledInMenu: false,
    menuName: "Introducing React",
    component: Loadable({
      loader: () => import("../pages/WhatIsReact"),
      loading: Loading
    }),
    head: {
      title: "Introducing React js",
      description: "Presenting React js and it's basic idea, uses, pros and cons."
    }
  },
  {
    url: "/editor-setup",
    urlExactMatch: true,
    inMainMenu: true,
    disabledInMenu: false,
    menuName: "Editor setup",
    component: Loadable({
      loader: () => import("../pages/EditorSetup"),
      loading: Loading
    }),
    head: {
      title: "Atom editor useful packages"
    }
  },
  {
    url: "/javascript2015",
    urlExactMatch: true,
    inMainMenu: true,
    disabledInMenu: false,
    menuName: "ECMAScript 6",
    component: Loadable({
      loader: () => import("../pages/Javascript2015"),
      loading: Loading
    }),
    head: {
      title: "New Javascript - ES6"
    }
  },
  {
    url: "/styling",
    urlExactMatch: true,
    inMainMenu: true,
    disabledInMenu: false,
    menuName: "Styling",
    component: Loadable({
      loader: () => import("../pages/Styling"),
      loading: Loading
    })
  },
  {
    url: "/routing",
    urlExactMatch: true,
    inMainMenu: true,
    disabledInMenu: false,
    menuName: "Routing",
    component: Loadable({
      loader: () => import("../pages/Routing"),
      loading: Loading
    })
  },
  {
    url: "/code-splitting",
    urlExactMatch: true,
    inMainMenu: true,
    disabledInMenu: false,
    menuName: "Code Splitting",
    component: Loadable({
      loader: () => import("../pages/CodeSplitting"),
      loading: Loading
    })
  },
  {
    url: "/state-management",
    urlExactMatch: true,
    inMainMenu: true,
    disabledInMenu: false,
    menuName: "Redux",
    component: Loadable({
      loader: () => import("../pages/Redux"),
      loading: Loading
    })
  },
  {
    url: "/forms",
    urlExactMatch: true,
    inMainMenu: true,
    disabledInMenu: false,
    menuName: "Forms (Practise)",
    component: Loadable({
      loader: () => import("../pages/Contact"),
      loading: Loading
    })
  },
  {
    url: "/snake",
    urlExactMatch: true,
    inMainMenu: true,
    disabledInMenu: false,
    menuName: "Snake (Practise)",
    component: Loadable({
      loader: () => import("../pages/SnakeGame"),
      loading: Loading
    })
  },
  {
    url: "/slideshow",
    urlExactMatch: true,
    inMainMenu: true,
    disabledInMenu: false,
    menuName: "Slideshow (Practise)",
    component: Loadable({
      loader: () => import("../pages/Slideshow"),
      loading: Loading
    })
  },
  {
    url: "/tetris",
    urlExactMatch: true,
    inMainMenu: false,
    disabledInMenu: false,
    menuName: "Tetris (Practise)",
    component: Loadable({
      loader: () => import("../pages/Tetris"),
      loading: Loading
    })
  },
  {
    url: "/coming-soon",
    urlExactMatch: false,
    inMainMenu: true,
    disabledInMenu: true,
    menuName: "Practise - coming soon",
    component: null
  },
  {
    path: "",
    urlExactMatch: false,
    inMainMenu: false,
    disabledInMenu: true,
    menuName: "",
    component: Loadable({
      loader: () => import("../pages/NotFound404"),
      loading: Loading
    })
  }
];

routes.forEach((route, index) => {
  if (!route.head) {
    route.head = {};
  }

  if (!route.head.title) {
    route.head.title = globalConfig.title;
  }

  if (!route.head.description) {
    route.head.description = globalConfig.description;
  }

  if (!route.layout) {
    route.layout = globalConfig.layout;
  }

  routes[index].head = (
    <Helmet>
      <title>{route.head.title}</title>
      <meta name="description" content={route.head.description} />
    </Helmet>
  );
});

export default routes;
