import * as actionTypes from "./actions";

const initialState = {
  isMenuVisible: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.OPEN_MENU:
      return {
        ...state,
        isMenuVisible: true
      };
    case actionTypes.CLOSE_MENU:
      return {
        ...state,
        isMenuVisible: false
      };
    case actionTypes.TOGGLE_MENU:
      return {
        ...state,
        isMenuVisible: !state.isMenuVisible
      };
    default:
      return state;
  }
};

export default reducer;
