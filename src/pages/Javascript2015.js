import React from "react";
import styled from "styled-components";

import SectionScroller from "../components/content/section-scroller/Basic";
import CodeHighlight from "../components/content/CodeHighlight";
import ReadMore from "../components/content/ReadMore";

const Javascript2015 = props => {
  const content = [
    {
      section: [
        <Content className="content" key="es6-versions">
          <div className="underline">
            EcmaScript 2015 | <span className="between-links">a.k.a</span> ECMAScript 6 <span className="between-links">a.k.a</span> ES2015{" "}
            <span className="between-links">a.k.a</span> ES6
          </div>
          <p>
            The new language features change the way you write a React app, making it easier and more fun than ever.<br />
            In fact it's more than recommended to use them. "The future is now".
          </p>
          <p className="description">
            The two "read more" links will tell you almost everything about ES6. <br />
            That's why the next sections will be more of a "best practises" type (for both JS and React).
          </p>
          <ReadMore link="https://www.w3schools.com/js/js_versions.asp" />
          <span className="between-links">and</span>
          <ReadMore link="https://babeljs.io/learn-es2015/" text="more" />
        </Content>
      ]
    },
    {
      anchor: "use-reduce",
      section: [
        <Content className="content" key="destruct-well">
          <div className="underline">Using “reduce” instead of “map” or “filter”</div>
          <p className="small">
            Suppose you have a situation where you have a list of items, and you want to update each item (that is, map) and then filter only a few items (that is,
            filter). But this means that you would need to run through the list twice!
          </p>

          <div className="content-box x2">
            <p className="small">
              In the below example, we want to double the value of items in the array and then pick only those that are greater than 50. Notice how we can use the
              powerful reduce method to both double (map) and then filter? That’s pretty efficient.
            </p>
            <CodeHighlight>{`
const numbers = [10, 20, 30, 40];
const doubledOver50 = numbers.reduce((finalList, num) => {

  // double each number (i.e. map)
  num = num * 2;

  // filter number > 50
  if (num > 50) {
    finalList.push(num);
  }

  return finalList;
}, []);

doubledOver50; // => [60, 80]
            `}</CodeHighlight>
          </div>
          <div className="content-box x2">
            <p className="small">
              There are times when you want to count duplicate array items or convert an array into an object. You can use reduce for that.<br />
              In the below example, we want to count how many cars of each type exist and put this figure into an object.
            </p>
            <CodeHighlight>{`
var cars = ['BMW','Benz', 'Benz', 'Tesla', 'BMW', 'Toyota'];

var carsObj = cars.reduce(function (obj, name) {
  obj[name] = obj[name] ? ++obj[name] : 1;
  return obj;
}, {});

carsObj; / / => { BMW: 2, Benz: 2, Tesla: 1, Toyota: 1 }
            `}</CodeHighlight>
          </div>
        </Content>
      ]
    },
    {
      anchor: "no-null",
      section: [
        <Content className="content" key="no-null">
          <div className="underline">No Ternary Operators With a Null Catch</div>
          <p>You wouldn’t write the following, would you?</p>
          <CodeHighlight>{`
if (booleanTest) {
  doSomething();
} else {}
          `}</CodeHighlight>
          <p>Therefore:</p>
          <CodeHighlight>{`
// do this
{ booleanTest && <MyComponent />}

// not this
{ booleanTest ? <MyComponent /> : null }
          `}</CodeHighlight>
        </Content>
      ]
    },
    {
      anchor: "minimize-functions",
      section: [
        <Content className="content" key="minimize-funcs">
          <div className="underline">Minimize Useless Class Functions</div>
          <CodeHighlight>{`
// do this
<input onChange={e => this.setState({'email', e.target.value }) />
<input onChange={e => this.setState({'pass', e.target.value }) />

// not this
emailChangeHandler = val => {
  this.setState({ email: val })
};

passwordChangeHandler = val => {
  this.setState({ pass: val })
};

<input onChange={this.emailChangeHandler} />
<input onChange={this.passwordChangeHandler} />
          `}</CodeHighlight>
        </Content>
      ]
    },
    {
      anchor: "destructure-well",
      section: [
        <Content className="content" key="destruct-well">
          <div className="underline">Destructure, But Don’t Over-Destructure</div>
          <p>
            Destructuring can also save a lot of redundant "this.props.xxx" declarations, but using it to grab a single property from something that gets called only once
            or twice adds little value.
          </p>
          <CodeHighlight>{`
// do this
import React, { Component } from 'react'
class MyComponent extends Component {}

// not this
import React from 'react'
class MyComponent extends React.Component {}
        `}</CodeHighlight>
          <CodeHighlight>{`
// do this
const { email, firstName, lastName, profile } = this.props.user;

// not this
const { children } = this.props
          `}</CodeHighlight>
        </Content>
      ]
    },
    {
      anchor: "fragments",
      section: [
        <Content className="content" key="wrap-fragment">
          <div className="underline">Fragments</div>
          <p>
            A common pattern in React is for a component to return multiple elements. Fragments let you group a list of children without adding extra nodes to the DOM.
          </p>
          <ReadMore link="https://reactjs.org/docs/fragments.html" />
          <CodeHighlight>{`
render() {
  return (
    <React.Fragment>
      <ChildA />
      <ChildB />
      <ChildC />
    </React.Fragment>
  );
}
          `}</CodeHighlight>
        </Content>
      ]
    },
    {
      anchor: "coming-soon",
      section: [
        <Content className="content" key="destruct-well">
          <div className="underline">Coming soon</div>
          <p>I'll keep adding more useful stuff here.</p>
        </Content>
      ]
    }
  ];

  return <SectionScroller content={content} />;
};
export default Javascript2015;

const Content = styled.div`
  .underline {
    margin-bottom: 35px;
  }

  p {
    &.description {
      font-size: 15px;
      font-weight: 600;
    }
    &.small {
      font-size: 15px;
    }
  }

  .slash {
    letter-spacing: -4px;
    display: inline-block;
    width: 19px;
    color: #3b7500;
  }
`;
