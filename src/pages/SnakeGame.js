import React, { Component } from "react";
import styled from "styled-components";

import api from "../utilities/api";
import backgroundImage from "../images/snake.svg";

const gameConfig = {
  snakeSize: 20,
  boardSize: 460,
  snakeSpeed: 120
};

class SnakeGame extends Component {
  state = {
    headPosition: {
      x: 0,
      y: 0
    },
    foodPosition: {
      x: 0,
      y: 0
    },
    score: 0,
    snakeTail: 0,
    moveDirection: "RIGHT",
    moveHistory: [],
    paused: false,
    gameOver: false,
    gameWin: false
  };

  initAnimationFrame = (time = 0) => {
    this.animationFrame = requestAnimationFrame(this.initAnimationFrame);

    if (time - this.lastUpdateTime >= gameConfig.snakeSpeed) {
      this.lastUpdateTime = time;

      this.moveSnake();
    }
  };

  startGame = () => {
    this.lastUpdateTime = 0;

    this.initAnimationFrame();

    if (this.state.gameOver) {
      this.setState(JSON.parse(JSON.stringify(this.initialState)), this.createFood);
    } else {
      this.createFood();
    }
  };

  endGame = () => {
    cancelAnimationFrame(this.animationFrame);
    this.setHighScore();
    this.setState({ gameOver: true });
  };

  winGame = () => {
    cancelAnimationFrame(this.animationFrame);
    this.setHighScore();
    this.setState({ gameWin: true });
  };

  pauseGame = () => {
    cancelAnimationFrame(this.animationFrame);
    this.setState({ paused: true });
  };

  resumeGame = () => {
    if (!this.state.gameOver && !this.state.gameWin) {
      this.initAnimationFrame();
      this.setState({ paused: false });
    }
  };

  setHighScore = () => {
    const highScore = api.localStorage.get("snakeHighScore");

    if (!highScore || highScore < this.state.score) {
      api.localStorage.set("snakeHighScore", this.state.score);
    }
  };

  createFood = () => {
    const coordinatesToIndex = (x, y, xMaxLength) => y * xMaxLength + (x + (y === 0 ? 0 : 1));
    const availableCells = [];
    const takenCells = {};

    takenCells[coordinatesToIndex(this.state.headPosition.x, this.state.headPosition.y, this.boardSideSize)] = true;

    if (this.state.snakeTail > 0) {
      this.state.moveHistory.forEach(coordinates => {
        takenCells[coordinatesToIndex(coordinates.x, coordinates.y, this.boardSideSize)] = true;
      });
    }

    for (let x = 0; x < this.boardSideSize; x++) {
      for (let y = 0; y < this.boardSideSize; y++) {
        if (!(coordinatesToIndex(x, y, this.boardSideSize) in takenCells)) {
          availableCells.push({ x: x, y: y });
        }
      }
    }

    if (availableCells.length <= 10) {
      this.winGame();
    } else {
      this.setState({ foodPosition: availableCells[Math.floor(Math.random() * availableCells.length)] });
    }
  };

  moveSnake = () => {
    const newHeadPosition = { ...this.state.headPosition };
    let newMoveHistory = [...this.state.moveHistory];
    let snakeTail = this.state.snakeTail;
    let foodIsEaten = false;
    let gameOver = false;
    const newState = {};

    if (this.state.gameWin) {
      return;
    }

    switch (this.state.moveDirection) {
      case "LEFT":
        newHeadPosition.x--;
        break;
      case "RIGHT":
        newHeadPosition.x++;
        break;
      case "UP":
        newHeadPosition.y--;
        break;
      case "DOWN":
        newHeadPosition.y++;
        break;
      default:
        return undefined;
    }

    if (newHeadPosition.x > this.boardEndPosition || newHeadPosition.x < 0 || newHeadPosition.y > this.boardEndPosition || newHeadPosition.y < 0) {
      gameOver = true;
    }

    if (newHeadPosition.x === this.state.foodPosition.x && newHeadPosition.y === this.state.foodPosition.y) {
      snakeTail++;
      foodIsEaten = true;
      newState["score"] = this.state.score + 20;
      newState["snakeTail"] = snakeTail;
    }

    if (snakeTail > 0) {
      newMoveHistory.push({ ...this.state.headPosition });

      newMoveHistory = newMoveHistory.slice(newMoveHistory.length - snakeTail, newMoveHistory.length);

      for (let i = 0; i < snakeTail; i++) {
        if (newMoveHistory[i].x === newHeadPosition.x && newMoveHistory[i].y === newHeadPosition.y) {
          gameOver = true;
        }
      }

      newState["moveHistory"] = newMoveHistory;
    }

    newState["headPosition"] = newHeadPosition;

    if (gameOver) {
      this.endGame();
    } else if (foodIsEaten) {
      this.setState(newState, this.createFood);
    } else {
      this.setState(newState);
    }
  };

  onKeyPress = e => {
    let key = e.keyCode;
    let restart = false;
    let togglePause = false;
    let changeDirection = false;

    if (key === 27) {
      togglePause = true;
    } else if (key === 13 || key === 32) {
      restart = true;
    } else if (key === 37 || key === 65) {
      changeDirection = this.state.moveDirection !== "RIGHT" ? "LEFT" : false;
    } else if (key === 39 || key === 68) {
      changeDirection = this.state.moveDirection !== "LEFT" ? "RIGHT" : false;
    } else if (key === 38 || key === 87) {
      changeDirection = this.state.moveDirection !== "DOWN" ? "UP" : false;
    } else if (key === 40 || key === 83) {
      changeDirection = this.state.moveDirection !== "UP" ? "DOWN" : false;
    }

    if (togglePause && !this.state.gameOver) {
      if (this.state.paused) {
        this.resumeGame();
      } else {
        this.pauseGame();
      }
    } else if (restart && this.state.gameOver) {
      this.startGame();
    } else if (changeDirection && changeDirection !== this.state.moveDirection && !this.state.gameOver && !this.state.paused) {
      this.lastUpdateTime = 0;

      cancelAnimationFrame(this.animationFrame);
      this.setState({ moveDirection: changeDirection }, this.initAnimationFrame);
    }
  };

  pieceCSSPosition = position => ({
    top: position.y * gameConfig.snakeSize,
    left: position.x * gameConfig.snakeSize
  });

  componentDidMount() {
    this.initialState = JSON.parse(JSON.stringify(this.state));
    this.boardSideSize = gameConfig.boardSize / gameConfig.snakeSize;
    this.boardEndPosition = this.boardSideSize - 1;

    this.startGame();

    window.addEventListener("keydown", this.onKeyPress, false);
  }

  componentWillUnmount() {
    cancelAnimationFrame(this.animationFrame);

    window.removeEventListener("keydown", this.onKeyPress, false);
  }

  render() {
    return (
      <div className="section">
        <div className="vertical-align">
          <GameWrap>
            <ScoreWrap>
              <Title>
                Retro Snake <span>with modern React</span>
              </Title>
              <Score>
                <strong>Score</strong> <span>{String(this.state.score).padStart(6, "0")}</span>
              </Score>
            </ScoreWrap>

            <Board>
              <Food style={this.pieceCSSPosition(this.state.foodPosition)} />
              <Snake className="head" style={this.pieceCSSPosition(this.state.headPosition)} />
              {this.state.moveHistory.map((position, index) => <Snake className="tail" style={this.pieceCSSPosition(position)} key={`tail-${index}`} />)}

              <GamePaused className="overlay" show={this.state.paused}>
                <div className="vertical-align">Paused</div>
              </GamePaused>

              <GameWin className="overlay" show={this.state.gameWin} onClick={this.startGame}>
                <div className="vertical-align">Congratulations, You Won!</div>
              </GameWin>

              <GameOver className="overlay" show={this.state.gameOver} onClick={this.startGame}>
                <div className="vertical-align">
                  <div className="high-score">High Score: {String(api.localStorage.get("snakeHighScore")).padStart(6, "0")}</div>
                  Click to restart<span>or press Space</span>
                </div>
              </GameOver>
            </Board>

            <Footer>
              <span>Pause/Resume</span>: Esc key. <span>Move</span>: arrow or "a, w, s, d" keys
            </Footer>
          </GameWrap>
        </div>
      </div>
    );
  }
}

export default SnakeGame;

const GameWrap = styled.div`
  display: table;
  margin: 0 auto;
`;

const ScoreWrap = styled.div`
  display: table;
  margin: 0 0 20px 0;
  min-width: 350px;
  width: ${gameConfig.boardSize}px;
`;

const Title = styled.h1`
  float: left;
  font-size: 18px;
  font-weight: 600;
  color: #405a4d;
  margin: 2px 0 0 0;
  font-style: italic;

  span {
    font-size: 13px;
    color: #bbbbbb;
    font-weight: 400;
  }
`;

const Score = styled.div`
  float: right;
  font-size: 16px;
  font-weight: 400;
  margin: 4px 0 0 0;

  strong {
    color: #405a4d;
    font-size: 11px;
    text-transform: uppercase;
  }

  span {
    font-weight: 500;
  }
`;

const Board = styled.div`
  position: relative;
  overflow: hidden;
  margin: 0 auto;
  background-color: #a5bdb6;
  border: 1px solid #65756b;
  box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.05);
  background-image: url(${backgroundImage});
  background-repeat: repeat;
  background-size: ${gameConfig.snakeSize}px;
  width: ${gameConfig.boardSize}px;
  height: ${gameConfig.boardSize}px;

  .overlay {
    position: absolute;
    display: table;
    width: 100%;
    height: 100%;
    left: 0;
    z-index: 2;
    color: #a5c5b4;
    font-weight: 600;
    font-size: 19px;
    cursor: pointer;
    text-align: center;
    text-transform: uppercase;
    background-color: rgba(37, 88, 75, 0.7);
  }
`;

const Food = styled.div`
  position: absolute;
  z-index: 1;
  background-color: #00864f;
  width: ${gameConfig.snakeSize}px;
  height: ${gameConfig.snakeSize}px;

  &::before {
    content: "";
    display: block;
    position: absolute;
    width: 50%;
    height: 50%;
    left: 18%;
    top: 19%;
    border: 1px solid #a5bdb6;
  }
`;

const Snake = styled.div`
  position: absolute;
  background-color: #0e1713;
  box-shadow: inset 0px 0px 3px #658a74;
  width: ${gameConfig.snakeSize}px;
  height: ${gameConfig.snakeSize}px;

  &::before {
    content: "";
    display: block;
    position: absolute;
    width: 50%;
    height: 50%;
    left: 18%;
    top: 19%;
    border: 1px solid #a5bdb6;
  }

  &.head {
    &::before {
      border-color: #d2eadc;
    }
  }
`;

const GameWin = styled.div`
  transition: top 0.3s ease;
  top: ${props => (props.show ? 0 : 100)}%;
`;

const GamePaused = styled.div`
  transition: opacity 0.3s ease;
  opacity: ${props => (props.show ? 100 : 0)};
  top: ${props => (props.show ? 0 : 100)}%;
`;

const GameOver = styled.div`
  transition: top 0.3s ease;
  top: ${props => (props.show ? 0 : 100)}%;

  span {
    display: block;
    font-size: 12px;
    margin: 8px 0 0 0;
  }

  .high-score {
    color: #ddffe8;
    font-size: 30px;
    font-weight: 400;
    margin: 0 0 20px 0;
  }
`;

const Footer = styled.div`
  font-size: 13px;
  color: #bbbbbb;
  font-weight: 400;
  text-align: center;
  font-style: italic;
  margin: 20px auto 0 auto;
  min-width: 350px;
  width: ${gameConfig.boardSize}px;

  span {
    color: #888;
  }
`;
