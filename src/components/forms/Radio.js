import React from "react";
import styled from "styled-components";

import Label from "./Label";

const Radio = props => (
  <InputRow className={`form-row${props.error ? " error" : ""}`}>
    {props.label && <Label label={props.label} error={props.error} />}
    <div className="radios-wrap">
      {props.buttons.map((item, index) => (
        <label className={`label-radio${props.error ? " error" : ""}${props.disabled ? " disabled" : ""}`} key={item.value}>
          <input type="radio" name={item.value} value={item.value} checked={props.checked[item.value].checked} onChange={props.onChange} disabled={props.disabled} />
          <span>{item.text}</span>
        </label>
      ))}
    </div>
  </InputRow>
);

export default Radio;

const InputRow = styled.div`
  position: relative;
  margin: 20px 0 0 0;

  .radios-wrap {
    margin: 10px 0 0 0;

    label {
      display: inline-block;
      color: #112146;
      text-align: left;
      font-size: 15px;
      cursor: pointer;
      font-weight: 400;
      margin: 0 0 0 15px;
      transition: color 0.3s ease;

      &.disabled {
        cursor: default;
        color: #888888;
      }
      &:first-child {
        margin-left: 0;
      }

      span {
        float: right;
        font-size: 13px;
        margin: 3px 0 0 0;
        font-weight: 500;
      }
    }
  }

  input {
    display: inline-block;
    vertical-align: top;
    cursor: pointer;
    margin: 4px 5px 0 0;
  }
`;
