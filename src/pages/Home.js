import React from "react";
import styled from "styled-components";

import animations from "../styles/animations";
import LogoSVG from "../images/LogoSVG";
import ReadMore from "../components/content/ReadMore";

const HomePage = props => (
  <Wrap>
    <LogoSVG />
    <div className="logo-text">React.js 16.4.0</div>
    <div className="author">
      Presentation and showcases created by <ReadMore link="https://github.com/mario-iliev" text="Mario Iliev" />
    </div>
  </Wrap>
);

export default HomePage;

const Wrap = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);

  &:hover {
    svg {
      path {
        fill: #17bed0;

        & + path {
          fill: #92bd00;
        }
      }
    }
  }

  svg {
    display: block;
    width: 140px;
    margin: 0 auto;
    animation: ${animations.spinInfinite} 10s linear infinite;

    path {
      fill: #00273c;
      transition: fill 0.4s ease;
    }
  }

  .logo-text {
    color: #00273c;
    font-size: 17px;
    text-align: center;
    margin: 20px 0 0 0;
    transition: border-color 0.5s ease;
    border-bottom: 1px solid transparent;
  }

  .author {
    color: #a5a1a1;
    font-size: 14px;
    a {
      text-transform: none;
      font-weight: 500;
    }
  }
`;
